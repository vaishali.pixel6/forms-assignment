import { Component, OnInit, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { FormGroup, FormArray, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';

import { ProfileService } from '../services/profile.service';

@Component({
	selector: 'app-address',
	templateUrl: './address.component.html',
	styleUrls: ['./address.component.css']
})

export class AddressComponent implements OnInit {

	@Input() data : any;
	@Input() actionType : string;
	@Output() saveAddress = new EventEmitter<FormGroup>();

	addressForm : FormGroup;
	subscription : any;
	zipSubscription : Subscription;

	constructor(private fb: FormBuilder, private profileService : ProfileService) { 
	    this.addressForm = this.fb.group({
			street: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(40), Validators.pattern(/^[#.0-9a-zA-Z\s,-]+$/)]],
			city: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(20), Validators.pattern(/^[A-Za-z\s]+$/)]],
			state: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(20), Validators.pattern(/^[A-Za-z\s]+$/)]],
			zip: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(6), Validators.pattern(/^[1-9]{1}[0-9]{5}$/)]]
	    });
  	}
  
	ngOnInit() {

	}

	ngOnChanges(changes: SimpleChanges){
		console.log(changes);
		if(changes.data.currentValue != changes.data.previousValue){ 
	  		this.addressForm.patchValue(changes.data.currentValue)
	  	}
	}

	save() {
		if(this.addressForm.valid){
			this.saveAddress.emit(this.addressForm);
		} else  {
			console.log("Address form INVALID");
		}
	}

	ngOnDestroy() {
		if(this.zipSubscription){
			this.zipSubscription.unsubscribe();	
		}
	}

}
