import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators, FormArray } from '@angular/forms';
import { ProfileService } from '../services/profile.service';

@Component({
	selector: 'app-profile-form',
	templateUrl: './profile-form.component.html',
	styleUrls: ['./profile-form.component.css']
})
export class ProfileFormComponent implements OnInit {
	
	profileForm : FormGroup;
  	isFormSubmitted: boolean;
  	showAddressForm: boolean;
  	editAddressObj: FormGroup;
  	editAddressIndex:number;
  	actionType: string;
  	isAddressPresent: boolean;
  
  	constructor(private fb: FormBuilder, private profileService : ProfileService) { 

		this.profileForm = this.fb.group({
			firstName: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(15), Validators.pattern(/^[A-Za-z]+$/)]],
			lastName: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(15), Validators.pattern(/^[A-Za-z]+$/)]],
			addresses : this.fb.array([])
		});		
		
  		this.isFormSubmitted = false;
		this.showAddressForm = true;
		this.editAddressIndex = -1;
		this.actionType = "ADD";
		this.isAddressPresent = false;
  	}  	

	ngOnInit() {

	}

	addNewAddress() {	
		this.actionType = "ADD";
		this.editAddressObj = null;
		this.editAddressIndex = -1;
		
		let control = this.profileForm.get('addresses') as FormArray;
		if(control.length < 5){
			this.showAddressForm = !this.showAddressForm;
		} else  {
			alert("Max 5 addresses.");
		}
	}

	editAddress(row,index) {
	 	this.actionType = "EDIT";
	 	this.showAddressForm = true;
	 	this.editAddressObj = row.value;
	 	this.editAddressIndex = index;
	}

	addOrUpdateAddress(grp) {	
	 	let control = this.profileForm.get('addresses') as FormArray;
	 	if(this.actionType == "EDIT") {
       		control.removeAt(this.editAddressIndex);
       		control.insert(this.editAddressIndex, grp);

	 	}else  if(this.actionType == "ADD") {
		  	if(control.length < 5) {
				control.push(grp);
		    	this.showAddressForm = !this.showAddressForm;
			}
	 	}
	  	this.showAddressForm = false;
	  	if(control.length > 0) {
	  		this.isAddressPresent = true;
	  	}
	}

	 removeAddress(index) {
	 	let control = <FormArray> this.profileForm.get('addresses');
	 	if(control.length){
	 		control.removeAt(index)
	 	}
	}

	get profileFormControl() { 
		return this.profileForm.controls; 
	} 

	onSubmit() {
		this.isFormSubmitted = true;
	}

	ngOnDestroy() {
	    console.log("OnDestroy");
	}

}
