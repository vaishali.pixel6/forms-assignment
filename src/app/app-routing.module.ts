import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { ProfileFormComponent } from './profile-form/profile-form.component';

const routes: Routes = [
		{ path: '', redirectTo: 'profile', pathMatch: 'full' },//redirect to profile component
		{ path: 'profile', component: ProfileFormComponent},
	];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
