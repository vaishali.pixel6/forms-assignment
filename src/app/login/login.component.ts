import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators, FormArray } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
	
	registrationForm : FormGroup;
  	isFormSubmitted = false;
  	canAddAddress = true;
  	numberOfAddresses = 0;

  
  	constructor(private fb: FormBuilder) { }  	

	ngOnInit(): void 
	{
		this.registrationForm = this.fb.group(
		{
			firstName: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(15), Validators.pattern(/^[A-Za-z]+$/)]],

			lastName: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(15), Validators.pattern(/^[A-Za-z]+$/)]],

			addresses : this.fb.array([this.initAddress()])
		});
	}

	initAddress()
	{
		let address = this.fb.group(
		{
     		street: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(40), Validators.pattern(/^[#.0-9a-zA-Z\s,-]+$/)]],

			city: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(20), Validators.pattern(/^[A-Za-z]+$/)]],

			state: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(20), Validators.pattern(/^[A-Za-z]+$/)]],

			zip: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(6), Validators.pattern(/^[1-9]{1}[0-9]{5}$/)]]
		});

		return address;
	}

	addNewAddress()
	{
		let formArray = <FormArray>this.registrationForm.get('addresses');
		formArray.push(this.initAddress());
		this.numberOfAddresses ++;
		if( this.numberOfAddresses > 4){
			this.canAddAddress = false;
		}
	}

	get registrationFormControl() { 
		return this.registrationForm.controls; 
	} 

	get addressFormControl(){
		return this.registrationForm['controls'].addresses['controls'];
	}

	onSubmit() 
	{
		this.isFormSubmitted = true;
	}

	ngOnDestroy(): void
	{
	    console.log("OnDestroy");
	}

}
